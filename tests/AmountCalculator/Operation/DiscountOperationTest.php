<?php
declare(strict_types=1);

namespace App\Tests\AmountCalculator\Operation;

use App\AmountCalculator\Operation\DiscountOperation;
use App\Catalog\Value\Amount;
use PHPUnit\Framework\TestCase;
use App\Catalog\Value\Discount;

use function PHPUnit\Framework\equalTo;

/** @covers \App\AmountCalculator\Operation\DiscountOperation */
final class DiscountOperationTest extends TestCase {

    /** @test */
    public function applyTo_WithMultipleDiscounts_ReturnsDiscountedAmount(): void {
        $operation = new DiscountOperation([
            Discount::fromAmount(10),
            Discount::fromAmount(10)
        ]);

        $expected = new Amount(80);
        $actual = $operation->applyTo(new Amount(100));
        $errorMessage = 
            "Expected discounted amount to be {$expected->getCents()} cents but was {$actual->getCents()} cents.";

        self::assertThat($expected, equalTo($actual), $errorMessage);
    }

    /** @test */
    public function applyTo_WithoutDiscounts_ReturnsOriginalAmount(): void {
        $operation = new DiscountOperation([]);

        $expected = new Amount(100);
        $actual = $operation->applyTo(new Amount(100));
        $errorMessage = 
            "Expected discounted amount to be {$expected->getCents()} cents but was {$actual->getCents()} cents.";

        self::assertThat($expected, equalTo($actual), $errorMessage);
    }

    /** @test */
    public function applyTo_WithDiscountGreaterThanAmount_ReturnsZero(): void {
        $operation = new DiscountOperation([
            Discount::fromAmount(40),
            Discount::fromAmount(70)
        ]);

        $expected = new Amount(0);
        $actual = $operation->applyTo(new Amount(100));
        $errorMessage = 
            "Expected discounted amount to be {$expected->getCents()} cents but was {$actual->getCents()} cents.";

        self::assertThat($expected, equalTo($actual), $errorMessage);
    }
    
}