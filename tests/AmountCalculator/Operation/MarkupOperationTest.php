<?php

declare(strict_types=1);

namespace App\Tests\AmountCalculator\Operation;

use PHPUnit\Framework\TestCase;
use App\AmountCalculator\Operation\MarkupOperation;
use App\Catalog\Value\Amount;


/** @covers \App\AmountCalculator\Operation\MarkupOperation */
final class MarkupOperationTest extends TestCase {

    /** @test */
    public function applyTo_WithMarkup_AddsPercentMarkup(): void {
        $operation = new MarkupOperation(.25);
        $amount = $operation->applyTo(new Amount(100));
        self::assertThat($amount, self::equalTo(new Amount(125)));
    }
   
    /** 
     * @test 
     * @dataProvider getInvalidMarkupValues
     * @param $markup
     */
    public function constructor_WithMarkupNotFloat_ThrowsException($markup): void {
        $this->expectException(\Exception::class);
        new MarkupOperation($markup);
    }

    public function getInvalidMarkupValues() {
        return [
            'int' => [1],
            'string' => ['0.25'],
            'boolean' => [true],
            'array' => [[]],
            'null' => [null],
            'object' => [new \stdClass()]
        ];
    }
}