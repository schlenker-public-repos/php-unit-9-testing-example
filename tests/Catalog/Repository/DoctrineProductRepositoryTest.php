<?php
declare(strict_types=1);

namespace App\Tests\Catalog\Repository;

use App\Catalog\Repository\DoctrineProductRepository;
use App\Catalog\Value\Amount;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\AbstractQuery;
use App\Tests\IntegrationTestCase;

use function PHPUnit\Framework\once;

/** @covers \App\Catalog\Repository\DoctrineProductRepository  */
final class DoctrineProductRepositoryTest extends IntegrationTestCase {
    
    private $entityManager;
    private $queryBuilder;
    private $query;

    public function setUp(): void {
        parent::setUp();
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->queryBuilder = $this->createMock(QueryBuilder::class);
        $this->query = $this->createMock(AbstractQuery::class);
    }

    /** @test */
    public function findProducts_WithoutDiscount_UsingMocks(): void {
        
        $product1 = new Product();
        $product1->name = 'Name 1';
        $product1->cost = 1000;
        $product1->markup = 10;

        $product2 = new Product();
        $product2->name = 'Name 2';
        $product2->cost = 1000;
        $product2->markup = 10;

        $this->query->expects($this->once())
            ->method('execute')
            ->willReturn([$product1, $product2]);
        
        $this->queryBuilder->expects($this->once())
            ->method('select')
            ->with('product', 'discount')
            ->willReturnSelf();

        $this->queryBuilder->expects($this->once())
            ->method('from')
            ->with(Product::class, 'product')
            ->willReturnSelf();

        $this->queryBuilder->expects($this->once())
            ->method('leftJoin')
            ->with('product.discounts', 'discount')
            ->willReturnSelf();

        $this->queryBuilder->expects($this->once())
            ->method('getQuery')
            ->willReturn($this->query);

        $this->entityManager->expects($this->once())
            ->method('createQueryBuilder')
            ->willReturn($this->queryBuilder);

        $dpr = new DoctrineProductRepository($this->entityManager);

        self::assertCount(
            2,
            $dpr->findProducts()
        );
    }

    /** @test */
    public function findProducts_WithoutDiscount_ReturnsFullPriceProducts(): void {
        $this->initializeContainer();
        $this->resetDatabase();
        $this->insertRecord('product', [
            'name' => 'Concert',
            'cost' => 1000,
            'markup' => 10
        ]);

        /** @var DoctrineProductRepository $repository */
        $repository = $this->diContainer->get(DoctrineProductRepository::class);

        self::assertEquals(
            [
                new \App\Catalog\Value\Product("Concert", new Amount(1100))
            ],
            $repository->findProducts()
        );
    }
}