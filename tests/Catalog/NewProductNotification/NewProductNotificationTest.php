<?php
declare(strict_types=1);

namespace App\Tests\Catalog\NewProductNotification;

use App\Catalog\NewProductNotification\NewProductNotification;
use Symfony\Component\Mailer\MailerInterface;
use App\Catalog\Repository\UserRepository;
use App\Catalog\Value\Amount;
use App\Catalog\Value\Product;
use Symfony\Component\Mime\RawMessage;
use Symfony\Component\Mailer\Envelope;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Mime\Address;

/** @covers \App\Catalog\NewProductNotification\NewProductNotification */
final class NewProductNotificationTest extends TestCase {
    
    private $mailerInterface;

    private $userRepository;

    public function setUp(): void {
        parent::setUp();
        $this->mailerInterface = $this->createStub(MailerInterface::class);
        $this->userRepository = $this->createStub(UserRepository::class);
    }

    /** @test */
    public function notifyUsersInArea_withUsersInArea(): void {

        $this->userRepository->method('findUsersInArea')->willReturn([
            ['email' => 'a@example.com'],
            ['email' => 'b@example.com']
        ]);

        $this->userRepository->method('findUsersInterestedInArtist')->willReturn([
            ['email' => 'a@example.com']
        ]);

        $this->mailerInterface->expects($this->exactly(2))
            ->method('send');

        $newProductNotification = new NewProductNotification(
            $this->mailerInterface, $this->userRepository
        );
        $product = new Product("Rubber Bands", new Amount(100));
        $newProductNotification->notifyUsers($product);
    }

    /** @test */
    public function notifyUsersInArea_withNoUsersInArea(): void {

        $this->userRepository->method('findUsersInArea')->willReturn([]);

        $this->mailerInterface->expects($this->never())
            ->method('send');

        $newProductNotification = new NewProductNotification(
            $this->mailerInterface, $this->userRepository
        );
        $product = new Product("James Taylor Live", new Amount(2000));
        $newProductNotification->notifyUsers($product);
    }

    /** @test */
    public function notifyUsersInArea_withUsersInArea_usingMailerSpy(): void {

        $this->userRepository->method('findUsersInArea')->willReturn([
            ['email' => 'a@example.com'],
            ['email' => 'b@example.com']
        ]);

        $this->userRepository->method('findUsersInterestedInArtist')->willReturn([
            ['email' => 'a@example.com']
        ]);

        $this->mailerInterface = $this->createMailerSpy();

        $newProductNotification = new NewProductNotification(
            $this->mailerInterface, $this->userRepository
        );
        $product = new Product("Big Bam Boom Tour", new Amount(1000));
        
        $newProductNotification->notifyUsers($product);

        $invocationRecipients = $this->mailerInterface->getSendInvocationRecipients();
        
        self::assertCount(
            2,
            $invocationRecipients
        );

        $duplicateRecipients = array_intersect(...$invocationRecipients);

        self::assertCount(
            1,
            $duplicateRecipients
        );
    }

    private function createMailerSpy(): MailerInterface {
        return new class implements MailerInterface {
            /** @var string[] */
            private array $sendInvocationRecipients;
            
            /**
             * @param \Symfony\Component\Mime\RawMessage $message
             * @param \Symfony\Component\Mailer\Envelope|null $envelope
             */
            public function send(
                RawMessage $message, 
                Envelope $envelope = null
            ): void {
                $this->sendInvocationRecipients[] = array_map(static function (Address $address) {
                    return $address->getAddress();
                }, $envelope->getRecipients());
            }

            /** @var string[] */
            public function getSendInvocationRecipients(): array {
                return $this->sendInvocationRecipients;
            }
        };
    }
}