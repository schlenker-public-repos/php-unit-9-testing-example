<?php
declare(strict_types=1);

namespace App\Tests\Catalog\Value;

use App\Catalog\Value\Amount;
use PHPUnit\Framework\TestCase;
use App\Catalog\Value\Discount;

use function PHPUnit\Framework\assertInstanceOf;
use function PHPUnit\Framework\equalTo;

/** @covers \App\Catalog\Value\Discount */
final class DiscountTest extends TestCase {

    public function setUp(): void {
        parent::setUp();
    }

    /** @test */
    public function fromAmount_ReturnsDiscount(): void {
        $actual = Discount::fromAmount(10);
        $errorMessage = 
            "Expected an object of type Discount, but it was something else.";
        self::assertInstanceOf(Discount::class, $actual);
    }

    /** @test */
    public function fromPercent_ReturnsDiscount(): void {
        $actual = Discount::fromPercent(50);
        $errorMessage = 
            "Expected an object of type Discount, but it was something else.";
        self::assertInstanceOf(Discount::class, $actual);
    }

    /** @test */
    public function getDiscountAmountForPrice_25DollarDiscountAmount_Returns25Dollars(): void {
        $discount = Discount::fromAmount(2500);

        $actual = $discount->getDiscountAmountForPrice(new Amount(10000));
        $errorMessage = 
            "Expected discount amount to be 2500 cents but was {$actual->getCents()} cents.";

        self::assertThat($actual->getCents(), equalTo(2500), $errorMessage);
    }

    /** @test */
    public function getDiscountAmountForPrice_25PercentDiscountOff100_Returns25Dollars(): void {
        $discount = Discount::fromPercent(0.25);

        $actual = $discount->getDiscountAmountForPrice(new Amount(10000));
        $errorMessage = 
            "Expected discount amount to be 2500 cents but was {$actual->getCents()} cents.";

        self::assertThat($actual->getCents(), equalTo(2500), $errorMessage);
    }
}