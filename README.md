# PHPUnit 9 Testing Example

This project is a modified version of an example project created for the Pluralsight course [Unit Testing with PHPUnit 9](https://app.pluralsight.com/library/courses/phpunit-testing-php). The project demonstrates the use of the PHPUnit testing framework. The tests may be run locally within an integrated development environment (IDE), or they may be run during the execution of the GitLab CI pipeline.

## Project Setup

Create your own copy of this project by forking it. Once the fork is complete, go into the settings of the forked project and remove the fork relationship (Settings -> General -> Advanced). Clone the project to your hard drive and import it into your integrated development environment (IDE). Open a terminal and cd into the project root. Once there, execute the command `make init`. Running this command fires up the docker containers, opens a bash shell, and executes a command to install composer and all the dependencies specified in composer.json. The `make init` command should only need to be run once on the project.

Once the `make init` command has completed, you can see a list of running docker containers by running the command `docker ps`. You should see two containers running: the php container and the mysql container. Now that the docker containers are running, you can execute the test suite locally by running the command `make test`. If you'd like to start the docker containers without running the tests, run the command `make start`. When you'd like to stop the docker containers, run the command `make stop`.

### Service Images

The project uses Docker Compose to run the tests locally. If you look in the file `docker-compose.yml`, you'll see that docker builds a network of two services: a `php` service and a `mysql` service. When the commands `docker-compose up` or `docker-compose run [command]` are executed by the Makefile script, Docker builds the `php` service image and the `mysql` service image from the dockerfiles located in the docker/php and docker/mysql directories, respectively. When the `php` service container starts, the entire project is mounted (loaded) into the container at the path `/var/www/phpunit`. Since the `php` container and the `mysql` container are part of the same network, the application is able to call from the php container to the database running in the `mysql` container.

In order for the service containers to be available when the GitLab CI/CD pipeline is run, both services need to be deployed to the GitLab Container Registry for your project. Perform the following steps to deploy both services to the GitLab Container Registry for the project:

#### Modify Image Registration Paths in .gitlab-ci.yml
Before you build and push the images to the GitLab container registry, you need to edit the image paths in .gitlab-ci.yml, located at the root of the project. Perform the following steps:
1. Open the file `.gitlab-ci.yml` in the editor.
2. The name of the php image can be found in the `services` section. The name of the php service is `registry.gitlab.com/schlenker-public-repos/php-unit-9-testing-example/phpunit-example-php:latest`. Change it to:
   - `registry.gitlab.com/my-group/my-project/my-custom-name-php:latest` where `my-group` is the name of the GitLab group where your project resides, `my-project` is the name of your project, and `my-custom-name` is a name that you create for the image.
   3. The name of the mysql service is `registry.gitlab.com/schlenker-public-repos/php-unit-9-testing-example/phpunit-example-mysql:latest`. Change it to:
   - `registry.gitlab.com/my-group/my-project/my-custom-name-mysql:latest` where `my-group` is the name of the GitLab group where your project resides, `my-project` is the name of your project, and `my-custom-name` is a name that you create for the image. Make sure that `my-custom-name` for the mysql service is the same as `my-custom-name` for the php service.
#### Build and Push php Image

1. Open the terminal. From the project root, cd into the `docker/php` directory.
2. Execute the following command to build the php service image and register it in the project specific GitLab Container Registry:

   - `docker build -t registry.gitlab.com/my-group/my-project/my-custom-name-php:latest .`
   - `docker login registry.gitlab.com` (Log in using GitLab credentials)
   - `docker push registry.gitlab.com/my-group/my-project/my-custom-name-php:latest`

   where `my-group` is the name of the GitLab group where your project resides, and `my-project` is the name of your project in GitLab. Don't forget the dot (period) at the end of the `docker build` command.
#### Build and Push mysql Image

1. Open the terminal. From the project root, cd into the `docker/mysql` directory.
2. Execute the following command to build the mysql service image and register it in the project specific GitLab Container Registry:

   - `docker build -t registry.gitlab.com/my-group/my-project/my-custom-name-db:latest .`
   - `docker login registry.gitlab.com` (Log in using GitLab credentials)
   - `docker push registry.gitlab.com/my-group/my-project/my-custom-name-db:latest`

   where `my-group` is the name of the GitLab group where your project resides, and `my-project` is the name of your project in GitLab. Don't forget the dot (period) at the end of the `docker build` command.

3. Open the file `gitlab-ci.yml` and under the `services` section, change the name of the `php` and `mysql` services to the path to your project-specific container registry

Once the `php` and `mysql` images have been built and registered in the project-specific GitLab Container Registry, and the service names are updated in `gitlab-ci.yml`, the GitLab CI/CD pipeline should run successfully when you push a commit to the remote repository.
## Local Testing

To run the phpunit tests in your local development environment, execute the command `make test` in a terminal at the root of the project. When you're done testing, execute the command `make stop` to shut down the docker containers.

## GitLab CI Pipeline Testing

Whenever changes to the project are committed and pushed to the remote GitLab repository, the CI/CD pipeline will run. During the pipeline run, the tests will be executed. If any of the tests fail, the pipeline will fail.
